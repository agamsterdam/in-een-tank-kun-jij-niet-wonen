# In een tank kun jij niet wonen

A documentary about “1980” in Amsterdam, by Jan Groen and Hanneke Willemse
Documentary originally recorded on super 8 about the squatters' movement in Amsterdam.

The documentary can always be found with the latest version of the subtitles at Kolektiva:
https://kolektiva.media/w/adCyZC4isHs5WQ55pqJqUk

Original link & bibliography:

+ https://archive.org/details/1981-in-een-tank-kun-je-niet-wonen
+ https://anarchistnews.org/content/anarcho-syndicalist-historian-hanneke-willemse-1949-2021-passes-away
+ https://www.krapuul.nl/samenleving/wonen-2/2759844/in-een-tank-kon-je-niet-wonen/


## About this repository

This repository is a way of enabling a collaborative translation and subtitling of the documentary "In een tank kun jij niet wonen".

You can help out with this project on several ways by:
+ Improving the translation of the existing languages.
+ Submitting subtitles for new languages.
+ Improving the syncing of the text with the audio.


## How to submit changes

Either open a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html) or send us over mail (aga@agamsterdam.org) your proposals.
